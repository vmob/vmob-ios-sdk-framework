# Plexure SDK framework

## Usage
For integrating Plexure SDK manually in the application.

## Requirements
Ensure the following iOS frameworks are linked to your project:
* MobileCoreServices.framework.
* SystemConfiguration.framework.
* CoreTelephony.framework.
* CoreLocation.framework.
* CoreBluetooth.framework.

## Installation
* Drag `VMobSDK.framework` on to your project and when prompted check the "Copy items into destination group's folder (if needed)" before pressing the "Finish" button.
* Drag `VMobSDKBundle.bundle` from VMobSDK.framework/Versions/A/Resouces folder on to your project and when prompted check the "Copy items into destination group's folder (if needed)" before pressing the "Finish" button
* Target -> Build Settings -> Other Linker Flags, set to `-ObjC`
* For more details, please refer: http://support.plexure.com/hc/en-us/articles/209679003-iOS

## Author

Kush Sharma, kush.sharma@plexure.com

## License
This SDK is provided under a restricted, non-transferrable license under the terms of Plexure's Standard Terms and Conditions as contained in your organization's commercial agreement with Plexure Limited. If your organization does not have a commercial agreement with Plexure, you are not permitted to download, access, or use this SDK in any way.
Subject to your compliance with these terms, we grant you a non-exclusive license to install and use the Software (a) in the Territory, (b) consistent with these terms and related documentation accompanying the SDK.