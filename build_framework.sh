#!/bin/bash

PUBLISH_CHANNEL=$1
SDK_VERSION=$2
FW_FILES_DIR=$3

FW_REPO_DIR="PlexureSDK_Frameworks"

# Copy frameworks to the correct dirs
printf "Copying frameworks to the correct dirs...\n"
cp "${FW_FILES_DIR}"/*_static_framework.tgz ${FW_REPO_DIR}/Static/
cp "${FW_FILES_DIR}"/*_dynamic_framework.zip ${FW_REPO_DIR}/Dynamic/

printf "\nPushing new SDK frameworks to Git...\n"
# We need to set the email and username or the push will fail
git config user.email "build@vmob.com"
git config user.name "BuildBot-v1.1.0"

git checkout ${PUBLISH_CHANNEL}
git add -A .
git commit -m "[VMob-SDK-Builder] Deployed VMob SDK framework version ${SDK_VERSION}"
git push origin ${PUBLISH_CHANNEL}

printf "\nAll done    (╯°□°）╯︵ ┻━┻\n"
